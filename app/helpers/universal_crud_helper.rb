module UniversalCrudHelper
  def normalize_field(locals)
    if locals[:field].is_a?(Hash) && locals[:field].dig(locals[:field].keys[0], :field_options)
      locals[:field_options] = locals[:field].dig(locals[:field].keys[0], :field_options)
      locals[:field] = locals[:field].keys[0]
    end
    render partial: 'field_normalize', locals: locals
  end
end
