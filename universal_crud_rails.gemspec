$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "universal_crud_rails/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "universal_crud_rails"
  s.version     = UniversalCrudRails::VERSION
  s.authors     = ["Beka Meshvildishvili"]
  s.email       = ["meshvildishvili@gmail.com"]
  s.homepage    = "https://gitlab.com/bekunika/universal_crud_rails"
  s.summary     = "Summary of UniversalCrudRails."
  s.description = "Description of UniversalCrudRails."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", ">= 6.0"

  s.add_development_dependency "sqlite3"
end
