require "universal_crud_rails/railtie"

module ActiveRecord
  class Base
    def build_single_relationship(relationship)
      send(relationship) || send("build_#{relationship}")
    end
  end
end

module UniversalCrudRails
  class Engine < Rails::Engine
  end

  module UniversalControllerActions
    # def define_model
    #   @model = Country
    #   @relationships = {
    #     categories: {
    #       label: name,
    #       many_to_many: true,
    #       as_check_boxes: true
    #     },
    #     images: {
    #       fields: [:file, :sort_order => { field_options: { disabled: true } }],
    #       hidden_fields: [:file_pseudo, :file_id_pseudo],
    #       # single: true,
    #       # allow_destroy: false
    #     },
    #     custom_field_values: {
    #       fields: [:custom_field, :field_value],
    #       relationships: {
    #         custom_field: {
    #           label: :name,
    #           collection: @model.new.tap{ |m| m.id = 0 }.custom_fields
    #         }
    #       }
    #     },
    #     country_type: {
    #       label: :name
    #     }
    #   }
    #   @general_fields = [
    #     [:categories, :country_type],
    #     [code => { field_options: { collection: [:one, :two] } }, :images, :custom_field_values]
    #   ]
    #   @translation_fields = [:name, :custom_field_values]
    #   @translation_relationships = {
    #     custom_field_values: {
    #       fields: [:custom_field, :field_value],
    #       relationships: {
    #         custom_field: {
    #           label: :name,
    #           collection: @model::Translation.new.tap{ |m| m.id = 0 }.custom_fields
    #         }
    #       }
    #     }
    #   }
    #   @internationalize_title = :name
    #   @fields_in_table = [:code]
    #   @items_on_page = 50
    #   @search_fields = [
    #     [
    #       { :id => { field_options: { as_range: true } } },
    #       :product_categories,
    #       { active: { field_options: { as_select: true } } },
    #     ],
    #     [
    #       :platform,
    #       { :generation => { field_options: { input_html: { multiple: false } } } },
    #       { created_at: { field_options: { as_range: true, input_html: { type: :date } } } },
    #       :product_categories,
    #       { :quantity => { field_options: { as_range: true, as: :numeric } } },
    #     ],
    #     [
    #       :name,
    #       :genre,
    #     ],
    #   ]
    #   @index_query = if params[:parent_id] then @model.rewhere(parent_id: params[:parent_id]) else @model.all end
    #   @sort_query = @model.rewhere(parent_id: params[:parent_id] ? params[:parent_id] : @model.rewhere(parent_id: nil).first)
    #   @sort_link_arguments = { :'parent_id' => params[:parent_id] }
    #   @action_buttons = {
    #         index: [
    #           view_context.link_to('X', view_context.send("#{@route_prefix}#{@model.to_s.underscore}_path", '-id-'), method: :delete, data: {confirm: 'Delete?'}, class: 'btn btn-danger'),
    #           view_context.link_to('>', view_context.send("#{@route_prefix}#{@model.to_s.pluralize.underscore}_path", parent_id: '-id-'), class: 'btn btn-secondary'),
    #         ],
    # end

    def index
      lookup_context.prefixes.push 'universal' unless lookup_context.prefixes.include? 'universal'
      search_conditions = @model.all

      additional_permitted_params = @translation_fields ? @translation_fields.clone : []
      relationships_field_map = {}
      search_selectboxes = []
      search_in_jsons = {}
      search_in_ranges = {}
      force_cast_types = {}

      @search_item = @model.new

      @search_fields.map! do |chunk|
        chunk.map! do |k|
          f = k.clone
          field_name = f.is_a?(Hash) ? f.keys[0] : f

          relationships_field_map[field_name] = relationship_to_field(field_name, @relationships.merge(@translation_relationships || {}))
          custom_field = nil

          if @model.respond_to?(:memoized_custom_fields) && @model.memoized_custom_fields.map(&:name).include?(field_name.to_s)
            custom_field = @model.memoized_custom_fields.select { |o| o.name.to_s == field_name.to_s }.first
            search_in_jsons[field_name] = :in_array if custom_field.field_type =='multiselectbox'
            f = custom_field.get_field_name_or_options
          end
          if @translation_fields && @model::Translation.respond_to?(:memoized_custom_fields) && @model::Translation.memoized_custom_fields.map(&:name).include?(field_name.to_s)
            custom_field = @model::Translation.memoized_custom_fields.select { |o| o.name.to_s == field_name.to_s }.first
            search_in_jsons[field_name] = :in_array if custom_field.field_type =='multiselectbox'
            f = custom_field.get_field_name_or_options
          end

          unless f.is_a?(Hash)
            f = { field_name => { field_options: {} } }
          end

          if k.is_a?(Hash) && k.dig(k.keys[0], :field_options) && f.is_a?(Hash) && f.dig(f.keys[0], :field_options)
            hash_to_merge = k[k.keys[0]][:field_options]
            f[field_name][:field_options].merge!(hash_to_merge) { |kk, x, y| x.is_a?(Hash) || [true, false].include?(x) || x.is_a?(Symbol) ? y : x + y } if f.is_a?(Hash)
          end

          if f.is_a?(Hash) && f.dig(f.keys[0], :field_options) && params.dig(:search_item, field_name).present?
            f[field_name][:field_options][:selected] = params.dig(:search_item, field_name)
          end

          search_selectboxes.push field_name  if custom_field && custom_field.field_type.in?(['selectbox', 'multiselectbox'])

          if relationships_field_map[field_name] && relationships_field_map.invert[field_name] != relationships_field_map[field_name]
            if params.dig(:search_item, relationships_field_map[field_name]).present?
              f[field_name][:field_options][:selected] = params.dig(:search_item, relationships_field_map[field_name])
            end

            search_selectboxes.push field_name
          end

          if field_name.in?(search_selectboxes) && (k.is_a?(Symbol) || k.dig(field_name, :field_options, :input_html, :multiple).nil?)
            f[field_name][:field_options][:input_html] ||= {}
            f[field_name][:field_options][:input_html][:multiple] = true
          end

          if k.is_a?(Hash) && k.dig(field_name, :field_options, :as_select).nil? && get_field_type_by_hierarchy!(field_name) == :boolean
            f[field_name][:field_options][:as_select] = true
          end

          if k.is_a?(Hash) && k.dig(field_name, :field_options, :as_select) === true && k.dig(field_name, :field_options, :collection).nil?
            f[field_name][:field_options][:collection] = [['', ''], ['Yes', 0], ['No', 1]]
          end

          if get_field_type_by_hierarchy!(field_name)[:type].in?([:datetime, :date, :time]) && f.dig(field_name, :field_options, :html5).nil?
            f[field_name][:field_options][:html5] = true
            f[field_name][:field_options][:force_as] = f[field_name][:field_options][:as] ||= get_field_type_by_hierarchy!(field_name)[:type]
            f[field_name][:field_options].delete(:as)
            f[field_name][:field_options][:input_html] ||= {}
            f[field_name][:field_options][:input_html][:type] = f[field_name][:field_options][:force_as] == :datetime ? :'datetime-local' : f[field_name][:field_options][:force_as]

            force_cast_types[field_name] = f[field_name][:field_options][:input_html][:type]

            f[field_name]
          end

          if f.is_a?(Hash) && f.dig(field_name, :field_options, :as_range)

            field_cloned_options = f.clone[field_name]
            if f.dig(field_name, :field_options, :as).nil?
              field_cloned_options[:field_options][:force_as] = get_field_type_by_hierarchy!(field_name.to_sym)[:type]
            end

            f[:from] = { "#{field_name}_from".to_sym => field_cloned_options }
            f[:to] = { "#{field_name}_to".to_sym => field_cloned_options }

            @search_item.class.send(:attr_accessor, "#{field_name}_from".to_sym)
            additional_permitted_params.push "#{field_name}_from".to_sym
            search_in_ranges["#{field_name}_from".to_sym] = { field_name: field_name, pair_key: "#{field_name}_to".to_sym, operator: :>=, param: :from }

            @search_item.class.send(:attr_accessor, "#{field_name}_to".to_sym)
            additional_permitted_params.push "#{field_name}_to".to_sym
            search_in_ranges["#{field_name}_to".to_sym] = { field_name: field_name, pair_key: "#{field_name}_from".to_sym, operator: :<=, param: :to }
          end
          f
        end
      end if @search_fields

      if defined?(@model::Translation) && @model::Translation.respond_to?(:memoized_custom_fields)
        instance = @search_item
        class << instance
          self::Translation.memoized_custom_fields.map(&:name).each do |custom_fields|
            attr_accessor custom_fields
          end
        end
      end

      if params[:search_item]
        params[:search_item].each_pair do |k, v|
          params[:search_item].delete(k) && next if !v || v == [] || v == [""]
          field_key = search_in_ranges[k.to_sym] ? search_in_ranges[k.to_sym][:field_name] : k.to_sym

          resolved_field = get_field_type_by_hierarchy!(field_key, v, relationships_field_map, additional_permitted_params)

          type = force_cast_types[field_key] || resolved_field[:type]
          relationship = resolved_field[:relationship]
          relationship_condition_key = resolved_field[:relationship_condition_key]

          case type
          when :string
            if relationship
              if relationship == :custom_field_values
                search_assotiation_condition = ["field_value ILIKE ? AND custom_field_id=?", "%#{v}%", @search_item.custom_field_id(k)]
              elsif relationship == [:translations, :custom_field_values]
                search_assotiation_condition = ["field_value ILIKE ? AND custom_field_id=?", "%#{v}%", @search_item.translation.custom_field_id(k)]
              else
                search_assotiation_condition = ["#{k} ILIKE ? ", "%#{v}%"]
              end
              search_conditions = search_conditions.where_assoc_exists(relationship, search_assotiation_condition)
            else
              search_conditions.where!("#{k} ILIKE ? ", "%#{v}%")
            end
          when :integer, :float, :datetime, :date, :time, :boolean
            cast_to_type = "::numeric"
            cast_to_type = "::timestamp" if type == :datetime
            cast_to_type = "::date" if type == :date
            cast_to_type = "::time" if type == :time
            cast_to_type = "::bollean" if type == :boolean

            v = v.to_f if cast_to_type == "::numeric" && !v.is_a?(Array) && relationship != [:translations, :custom_field_values] && relationship != :custom_field_values
            v = v.to_datetime if cast_to_type == "::timestamp"
            v = v.to_date if cast_to_type == "::date"
            v = v.to_time if cast_to_type == "::time"

            condition_hash = ["#{relationship_condition_key ? relationship_condition_key : k}#{cast_to_type} = ?", v]
            condition_hash = { relationship_condition_key ? relationship_condition_key : k => v } if v.is_a?(Array)

            if relationship
              if relationship == :custom_field_values
                condition_hash = { :field_value => v, :custom_field_id => @search_item.custom_field_id(field_key.to_s) }
                if search_in_jsons[relationships_field_map.invert[field_key]] == :in_array
                  condition_hash = ["field_value::jsonb ?| ARRAY[:in_array] AND custom_field_id=:custom_field_id", { in_array: v, custom_field_id: @search_item.custom_field_id(field_key.to_s)}]
                end
                if search_in_ranges[k.to_sym]
                  condition_hash = ["field_value#{cast_to_type} #{search_in_ranges[k.to_sym][:operator]} ? AND custom_field_id=?", v, @search_item.custom_field_id(search_in_ranges[k.to_sym][:field_name].to_s)]
                end
              elsif  relationship == [:translations, :custom_field_values]
                condition_hash = { :field_value => v, :custom_field_id => @search_item.translation.custom_field_id(field_key.to_s) }
                if search_in_jsons[relationships_field_map.invert[field_key]] == :in_array
                  condition_hash = ["field_value::jsonb ?| ARRAY[:in_array] AND custom_field_id=:custom_field_id", { in_array: v, custom_field_id: @search_item.translation.custom_field_id(field_key.to_s)}]
                end
                if search_in_ranges[k.to_sym]
                  condition_hash = ["field_value#{cast_to_type} #{search_in_ranges[k.to_sym][:operator]} ? AND custom_field_id=?", v, @search_item.translation.custom_field_id.custom_field_id(search_in_ranges[k.to_sym][:field_name].to_s)]
                end
              end
              search_conditions = search_conditions.where_assoc_exists(relationship, condition_hash)
            else
              if search_in_ranges[k.to_sym]
                condition_hash = ["#{search_in_ranges[k.to_sym][:field_name]}#{cast_to_type} #{search_in_ranges[k.to_sym][:operator]} ? ", v]
              end
              search_conditions.where!(condition_hash)
            end
          end if v.present?
        end

        permitted = item_params(:search_item, additional_permitted_params)

        @search_item.assign_attributes permitted
      end

      @items = @model.merge(@index_query || @model.all)
      @items = @items.merge(search_conditions) if params[:search_item]
      @items = @items.order(created_at: :desc)
                     .page(params[:page])
                     .per(@items_on_page)
    end


    def new
      lookup_context.prefixes.push 'universal' unless lookup_context.prefixes.include? 'universal'
      @item = @model.new { |p| p.unique_id = generate_unique_id }
    end

    def create
      lookup_context.prefixes.push 'universal' unless lookup_context.prefixes.include? 'universal'
      save_status = @model.transaction do
        @item = @model.new(item_params)
        save_status = @item.save
        raise ActiveRecord::Rollback unless save_status
        save_status
      end

      if save_status
        flash[:success] = "#{@model.to_s} Created"
        if params[:commit_and_reload]
          redirect_to send("edit_#{@route_prefix}#{@model.to_s.underscore}_path", @item)
        else
          redirect_to send("#{@route_prefix}#{@model.to_s.underscore.pluralize}_path")
        end
      else
        # p @item.errors
        render :new
      end
    end

    def edit
      lookup_context.prefixes.push 'universal' unless lookup_context.prefixes.include? 'universal'
    end

    def update
      lookup_context.prefixes.push 'universal' unless lookup_context.prefixes.include? 'universal'
      save_status = @model.transaction do
        @item.assign_attributes(item_params)
        save_status = @item.save
        raise ActiveRecord::Rollback unless save_status
        save_status
      end

      if save_status
        flash[:success] = "#{@model.to_s} Saved"
        if params[:commit_and_reload]
          redirect_to send("edit_#{@route_prefix}#{@model.to_s.underscore}_path", @item)
        else
          redirect_to send("#{@route_prefix}#{@model.to_s.underscore.pluralize}_path")
        end
      else
        # p @item.errors
        render :edit
      end
    end

    def destroy
      lookup_context.prefixes.push 'universal' unless lookup_context.prefixes.include? 'universal'
      @item.destroy
      redirect_to send("#{@route_prefix}#{@model.to_s.underscore.pluralize}_path")
    end

    def sort
      lookup_context.prefixes.push 'universal' unless lookup_context.prefixes.include? 'universal'
      @items = @model.merge(@sort_query || @model.all).order(created_at: :desc).includes([]).page(params[:page]).per(@items_on_page)
      render :index
    end

    def save_sort
      @model.transaction do
        params[@model.to_s.underscore].each_with_index do |id, index|
          save_status = @model.find(id).update(position: index) # TODO custom sort column
          raise ActiveRecord::Rollback unless save_status
          save_status
        end
      end
      render status: :ok, json: @controller.to_json
    end

    private

    def item_params(model = nil, allowed_extra = [])
      model = @model.to_s.underscore.to_sym if model === nil
      params.require(model).permit(@permitted_ary + allowed_extra)
    end

    def find_item
      @item = @model.find_by_id(params[:id])
      render_404 unless @item
    end

    def normalize_model_data
      @general_fields.map! { [_1] } unless @general_fields.all? { |element| element.is_a?(Array) }
      if @translation_fields.present?
        @translation_fields.map! { [_1] } unless @translation_fields.all? { |element| element.is_a?(Array) }
      end
      @relationships.each do |key, relationship|
        if @relationships[key][:fields] && !@relationships[key][:fields].all? { |element| element.is_a?(Array) }
          @relationships[key][:fields] = @relationships[key][:fields].map! { [_1] }
        end
      end

      if @translation_fields.present?
        @translation_relationships.each do |key, relationship|
          if @translation_relationships[key][:fields] && !@translation_relationships[key][:fields].all? { |element| element.is_a?(Array) }
            @translation_relationships[key][:fields] = @translation_relationships[key][:fields].map! { [_1] }
          end
        end
        @permitted_ary = [translations_attributes: [:id, :locale, :_destroy] + get_permitted_ary_for(@translation_fields, @translation_relationships, @model.new.translation.class)]
      else
        @permitted_ary = []
      end
      @permitted_ary += get_permitted_ary_for(@general_fields, @relationships, @model)
    end

    def get_permitted_ary_for(fields_list, relationships, model)
      fields_list = fields_list.flatten(1) if fields_list.all? { |element| element.is_a?(Array) }

      permitted_ary = []

      fields_list.each do |f|
        permitted_ary << if relationships.dig(f, :fields)
                           fields = relationships[f][:fields]
                           fields = relationships[f][:fields].flatten(1) if relationships[f][:fields].all? { |element| element.is_a?(Array) }
                           fields = fields.map do |nf|
                             relationships[f].dig(:relationships, nf) ? nf.to_s.singularize.+('_id').to_sym : nf
                           end + [:id] + (relationships[f][:hidden_fields] || [])
                           fields.map! { |f| normalize_permitted_field(f, model) }
                           fields.append(:_destroy) unless relationships[f][:allow_destroy] === false
                           { f.to_s.+('_attributes').to_sym => fields }
                         elsif relationships[f] && relationships[f][:many_to_many]
                           { relationship_to_field(f, relationships) => [] }
                         elsif relationships[f]
                           relationship_to_field(f, relationships)
                         elsif !f.is_a?(Hash) && model.new.send(f.to_sym).kind_of?(ActiveStorage::Attached::Many)
                           { f.to_sym => [] }
                         else
                           normalize_permitted_field(f, model)
                         end
      end
      permitted_ary
    end

    def relationship_to_field(field, relationships = {})
      if relationships[field] && relationships[field][:many_to_many]
        field.to_s.singularize.+('_ids').to_sym
      elsif relationships[field]
        field.to_s.singularize.+('_id').to_sym
      else
        field
      end
    end

    def normalize_permitted_field(f, model)
      if f.is_a?(Hash) && f.dig(f.keys[0], :field_options, :disabled)
        nil
      elsif f.is_a?(Hash) && f.dig(f.keys[0], :field_options, :input_html, :multiple) === true
        { f.keys[0] => [] }
      elsif f.is_a?(Hash) && f.dig(f.keys[0], :field_options)
        f.keys[0]
      else
        f
      end
    end

    def get_field_type_by_hierarchy!(field_key, field_value = nil, relationships_field_map = {}, additional_permitted_params = [])
      relationship = nil
      relationship_condition_key = nil
      found_in = nil

      type = @model.type_for_attribute(field_key)&.type

      found_in = :main if type

      if !type && @translation_fields && @model::Translation.type_for_attribute(field_key.to_s)&.type
        type = @model::Translation.type_for_attribute(field_key.to_s)&.type
        relationship = :translations
        found_in = :translations_main
      end
      if @model.respond_to?(:memoized_custom_fields) && @model.memoized_custom_fields.map(&:name).include?(field_key.to_s) && !type
        type = @model.new.type_for_attribute(field_key.to_s)&.type
        relationship = :custom_field_values
        found_in = :custom_fields
      end
      if @translation_fields && @model::Translation.respond_to?(:memoized_custom_fields) && @model::Translation.memoized_custom_fields.map(&:name).include?(field_key.to_s) && !type
        type = @model.new.translation.type_for_attribute(field_key.to_s)&.type
        relationship = [:translations, :custom_field_values]
        found_in = :translations_custom_fields
      end

      if relationship || relationships_field_map.invert[field_key]
        type = :integer if field_value.is_a?(Array)
        if @relationships.dig(relationships_field_map.invert[field_key], :many_to_many)
          relationship = relationships_field_map.invert[field_key]
          relationship_condition_key = :id
        end
        additional_permitted_params.push(field_value.is_a?(Array) ? { field_key.to_s => [] } : field_key.to_s)
      end

      { field: field_key, type: type, relationship: relationship, relationship_condition_key: relationship_condition_key, found_in: found_in }
    end
  end
end
